from dagster import ModeDefinition, pipeline, PresetDefinition
from cereal_processing.solids.step_5 import display_results, load_cereals_step5
from cereal_processing.solids.step_5 import sort_by_calories_step5, sort_by_protein_step5

MODE_DEV = ModeDefinition(name="dev", resource_defs={})
MODE_TEST = ModeDefinition(name="test", resource_defs={})

@pipeline(mode_defs=[MODE_DEV, MODE_TEST])
def step5_pipeline():
    cereals = load_cereals_step5()
    display_results(
        most_calories=sort_by_calories_step5(cereals),
        most_protein=sort_by_protein_step5(cereals),
    )