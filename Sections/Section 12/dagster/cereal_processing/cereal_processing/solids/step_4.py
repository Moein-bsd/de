import csv
import os

from dagster import execute_pipeline, pipeline, solid, OutputDefinition

@solid(config_schema={"csv_name": str})
def read_csv(context):
    csv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), f"data{os.path.sep}{context.solid_config['csv_name']}")
    context.log.info(f"File : {csv_path}")
    with open(csv_path, "r") as fd:
        lines = [row for row in csv.DictReader(fd)]

    context.log.info(f"Found {len(lines)} lines".format())
    return lines

@solid
def sort_by_calories_step4(context, cereals):
    sorted_cereals = list(
        sorted(cereals, key=lambda cereal: cereal["calories"])
    )

    context.log.info(f'Most caloric cereal: {sorted_cereals[-1]["name"]}')
    return sorted_cereals