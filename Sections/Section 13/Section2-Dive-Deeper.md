####  [Simple query string query](https://github.com/elastic/elasticsearch/edit/7.12/docs/reference/query-dsl/simple-query-string-query.asciidoc)

Returns documents based on a provided query string, using a parser with a limited but fault-tolerant syntax. **A simpler, more robust version of the `query_string` syntax suitable for exposing directly to users.**

```json
GET movies/_search
{
  "query": {
    "simple_query_string" : {
        "query": "kill +(bill | mockingbird) -crime",
        "fields": ["title^5", "director"],
        "default_operator": "and"
    }
  }
}

or 

GET movies/_search
{
  "query": {
    "simple_query_string" : {
        "query": "kill +(bill | mockingbird) -to",
        "fields": ["title^5", "director"],
        "default_operator": "and"
    }
  }
}
```



While its syntax is more limited than the [`query_string` query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html), the `simple_query_string` query does not return errors for invalid syntax. Instead, it ignores any invalid parts of the query string.



#### Pagination

- we need to specify size & from parameters

![Size & From](https://i.postimg.cc/R0v4LZb4/Pagination.png)

```json

POST movies/_search
{
  "size": 2,
  "from": 2, 
  "query": {
    "term": {
      "genres": {
        "value": "drama"
      }
    }
  }
}
```

##### Beware

- don't use deep pagination
- set upper bound to end users

#### Sorting

```json
POST movies/_search
{
  "sort": [
    {
      "year": {
        "order": "desc"
      }
    }
  ], 
  "query": {
    "term": {
      "genres": {
        "value": "drama"
      }
    }
  }
}
```

- sort not applicable for text fields 

[![image.png](https://i.postimg.cc/nL651Hk0/image.png)](https://postimg.cc/d7j4scgy)

- use `Keyword` fields .

```json
POST movies/_search
{
  "sort": [
    {
      "year": {
        "order": "asc"
      },
      "title.keyword": {
        "order": "desc"
      }
    }
  ],
  "query": {
    "term": {
      "genres": {
        "value": "drama"
      }
    }
  }
}
```

##### Beware

- ES dose scoring for you and it's better to use that instead of `sorting`

#### [Exist Query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-exists-query.html)

Returns documents that contain an indexed value for a field.

```json
GET movies/_search
{
  "query": {
    "exists": {
      "field": "city"
    }
  }
}

or 

GET movies/_search
{
  "query": {
    "exists": {
      "field": "city.location"
    }
  }
}
```

- find all documents that not contain `city` field : 

```json
GET movies/_search
{
  "query": {
    "bool": {
      "must_not": {
        "exists": {
          "field": "city"
        }
      }
    }
  }
}
```

#### [Prefix Queries](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-prefix-query.html)

```json
GET /movies/_search
{
  "query": {
    "prefix" : { "title": "the" }
  }
}
```

- You can speed up prefix queries using the [`index_prefixes`](https://www.elastic.co/guide/en/elasticsearch/reference/current/index-prefixes.html) mapping parameter

###### [Match Phrase Prefix Query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query-phrase-prefix.html)

```json
GET /movies/_search
{
  "query": {
    "match_phrase_prefix": {
      "title": {
        "query": "kill b"
      }
    }
  }
}
```

#### [Wildcard Queries](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-wildcard-query.html)

| **Wildcard** | **Description**          |
| ------------ | ------------------------ |
| *            | Matches any character    |
| ?            | Matches single character |

```json

GET movies/_search
{
  "query": {
    "wildcard": {
      "title": {
        "value": "Apo*"
      }
    }
  }
}

or 


GET movies/_search
{
  "query": {
    "wildcard": {
      "title": {
        "value": "No?"
      }
    }
  }
}
```

#### [Regexp Queries](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-regexp-query.html)

- more advanced patterns

```json
GET /my_index/address/_search
{
    "query": {
        "regexp": {
            "postcode": "W[0-9].+"
        }
    }
}
```



***Allow expensive queries***

Wildcard and Regexp  queries will not be executed if [`search.allow_expensive_queries`](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html#query-dsl-allow-expensive-queries) is set to false.



`prefix`, `wildcard` and `regexp`queries all work the same internally by creating an automaton that describes matching terms and intersecting it with the terms dictionary.

`SELECT * FROM table WHERE name.raw LIKE 'Product1*'` can be queried : 

```json
{"query":{"prefix":{"name.keyword":"Product1"}}}

{"query":{"wildcard":{"name.keyword":"Product1*"}}}

{"query":{"regexp":{"name.keyword":"Product1.*"}}}

{"query":{"bool":{"must":{"prefix":{"name.keyword":"Product1"}}}}}

{"query":{"bool":{"must":{"wildcard":{"name.keyword":"Product1*"}}}}}

{"query":{"bool":{"must":{"regexp":{"name.keyword":"Product1.*"}}}}}

{"query":{"filtered":{"filter":{"bool":{"must":{"regexp":{"name.keyword":"Product1.*"}}}}}}}
```

**Beware**

`prefix`, `wildcard` and `regexp`queries  are expensive and try not to use them. if you are forced to leveraging them, change the mapping of underlying fields to reduce the cost. (for example use `wildcard` field type)



#### [Terms Queries](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-terms-query.html)

Returns documents that contain one or more **exact** terms in a provided field. The `terms` query is the same as the [`term` query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-term-query.html), except you can search for multiple values.

```json
GET /movies/_search
{
  "query": {
    "terms": {
      "genres": [
        "drama",
        "war"
      ]
    }
  }
}
```



#### [Script Queries](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-script-query.html)

Filters documents based on a provided [script](https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-scripting-using.html). The `script` query is typically used in a [filter context](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-filter-context.html).

```json
GET movies/_search
{
  "query": {
    "bool": {
      "filter": {
        "script": {
          "script": """
            double year = doc['year'].value;
            if (year%2 == 1) {
             return true;
            }
            return false ;
          """
        }
      }
    }
  }
}
```

######  Allow expensive queries

Script queries will not be executed if [`search.allow_expensive_queries`](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html#query-dsl-allow-expensive-queries) is set to false.

