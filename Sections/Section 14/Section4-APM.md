#### APM Server Up & Running



#### APM Server Config 

- run the `APM Server`
  - `docker-compose.exe -f .\docker-compose-apm-server.yml up`

`apm-server.yml`:

```yaml
apm-server:
  host: 0.0.0.0:8200
  ssl.enabled: false
  ssl.verification_mode: none
output:
  elasticsearch:
    hosts: ['http://elasticsearch:9200']
    username: elastic
    password: changeme
    ssl.enabled: false

```

- sample config file :  [elastic-apm-server/apm-server.reference.yml at master · yidinghan/elastic-apm-server (github.com)](https://github.com/yidinghan/elastic-apm-server/blob/master/apm-server.reference.yml)

#### Handling Exceptions

- a sample code (‍`/codes/ingest_tweets_apm_exceptions.py`) :

```python
import elasticapm

clientAPM = elasticapm.Client(
            service_name='Tweets',
            service_version="1.0",
            # hostname="localhost"
            # service_node_name="crawler",
            # server_url=apm_url,
            )


def insert_es(index,id,body) :
    try:
        # ERROR!
        res = es.index(index, id, body)
        # Correct Version
        # res = es.index(index=index, id=id, body=body)
        return True
    except Exception as e:
        print("es exception: " + str(e))
        clientAPM.capture_exception()
        return False
    
.
.
.
clientAPM.capture_message("Response code error: " + str(result))
.
.
```

- `pip install -r requirements.txt` 
- run the `codes/ingest_tweets_apm_exceptions.py`
- check out the `kibana` - `APM Section`

![APM Exceptions](https://i.postimg.cc/fy8x6FS9/image.png)



#####  Transactions & Capture Spans

- We can measure how long each operation/function will take  using **transactions**
  - inside each transaction, we can measure **spans** separately.
- see some samples :

```python
if result == requests.codes.ok:
                clientAPM.begin_transaction(transaction_type="script")
                tweets = response.json()['items']
         		.
                .
                .
                clientAPM.end_transaction(name="Get & Save Sahamyab Tweets", result="success")
                
@elasticapm.capture_span("Get Hashtags")
def get_hashtags(tweet) :
    .
    .
    .

@elasticapm.capture_span("Insert To ES")
def insert_es(index,id,body) :
    .
    .
    .
    
```

- result="success" or whatever you want.

- run the `codes/ingest_tweets_apm_spans.py`

![Transactions & Capture Spans](https://i.postimg.cc/6QbWtV7M/image.png)



- We can see details of every Span

  

![](https://i.postimg.cc/FKyzvkyq/image.png)

- try to use async version 



