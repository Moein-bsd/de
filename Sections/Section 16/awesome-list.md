- [infoslack/awesome-kafka: A list about Apache Kafka (github.com)](https://github.com/infoslack/awesome-kafka)

- [streamthoughts/awesome-opensource-contribs-kafka: A list of all awesome open-source contributions for the Apache Kafka project (github.com)](https://github.com/streamthoughts/awesome-opensource-contribs-kafka)

- [gunnarmorling/awesome-opensource-data-engineering: An Awesome List of Open-Source Data Engineering Projects (github.com)](https://github.com/gunnarmorling/awesome-opensource-data-engineering/)

  