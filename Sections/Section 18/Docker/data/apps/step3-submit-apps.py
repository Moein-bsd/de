#!/usr/bin/env python
# coding: utf-8


from pyspark.sql import SparkSession
from pyspark.sql.functions import *



def init_spark():
  sql = SparkSession.builder.master("spark://spark-master:7077").appName("Step3-Submit-App")\
        .config("spark.executor.memory", "500mb")\
        .config("spark.jars", "/opt/spark-apps/postgresql-42.2.22.jar").getOrCreate()
  sc = sql.sparkContext
  return sql,sc

spark,sc = init_spark()



def main():
  global   spark,sc   
  url = "jdbc:postgresql://postgres-server:5432/mta_data"
  properties = {
    "user": "nikamooz",
    "password": "nikamooz123",
    "driver": "org.postgresql.Driver"
  }
  file = "/opt/spark-data/MTA-Bus-Time_.2014-08-01.txt"


  df = spark.read.load(file,format = "csv", inferSchema="true", sep="\t", header="true"
      ) \
      .withColumn("report_hour",date_format(col("time_received"),"yyyy-MM-dd HH:00:00")) \
      .withColumn("report_date",date_format(col("time_received"),"yyyy-MM-dd"))
  
  # Filter invalid coordinates
  df.where("latitude <= 90 AND latitude >= -90 AND longitude <= 180 AND longitude >= -180")     .where("latitude != 0.000000 OR longitude !=  0.000000 ")     .write     .jdbc(url=url, table="mta_reports", mode='append', properties=properties)     .save()
  
if __name__ == '__main__':
  main()
