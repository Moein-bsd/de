### Setup Spark Cluster with Docker

#### Build Basic Images

- open cmd and go to `docker/build` folder

- build `Spark` primary image :

  ```bash
  $ docker build -t nikamooz/spark:3.1.2 ./spark
  ....
  $ docker image ls
  ```

  

- build `pyspark` image based on previous image 

  ```bash
  $ docker build -t nikamooz/pyspark:3.1.2 ./spark
  ....
  $ docker image ls
  ```

- if you want to submit your python modules along with your submit command , refer to this address : 

   [How to Manage Python Dependencies in Spark - The Databricks Blog](https://databricks.com/blog/2020/12/22/how-to-manage-python-dependencies-in-pyspark.html)

### Setup The Main Cluster

- go back to `docker` folder and setup the spark cluster with `docker-compose` :

  ```bash
  $ docker-compose up -d
  $ docker-compose ps
  $ docker-compose logs -f 
  $ docker-compose logs -f pyspark
  
  ```

- click on the link provided in `pyspark` logs.

- do not forget to add spark/hadoop host names to local DNS.

  - Linux : `sudo /etc/hosts`

  - Windows : open note pad as administrator , the open the `c:\Windows\System32\drivers\etc\hosts` 

  - add these lines :

    ```bash
    127.0.0.1	spark-master
    127.0.0.1	spark-worker1
    127.0.0.1	spark-worker2
    127.0.0.1	pyspark
    127.0.0.1	namenode
    127.0.0.1	datanode
    ```

    

- run the sample notebook provided.

#### Setup The Spark with HDFS Support

- `docker-compose-hadoop.yml` is responsible for enabling `HDFS` support for our spark cluster

  ```bash
  $  docker-compose -f .\docker-compose-hadoop.yml up -d
  ```

  - go to `http://namenode:9870` and check out that everything is OK

  - first , create /data/csv folder 
    - `docker exec -it namenode bash`
  
    ```bash
    $ cd /data
    $ hadoop fs -mkdir -p /data/csv
    $ hadoop fs -put ./csv/* /data/csv
    ```
