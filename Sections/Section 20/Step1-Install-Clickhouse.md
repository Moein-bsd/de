#### Installation Guide 

***Main Reference :*** [Installation | ClickHouse Documentation](https://clickhouse.com/docs/en/getting-started/install/)

open WSL terminal / Ubuntu CLI (`clickhouse` only supports `Linux`) :

- set password : `default123` for user default

  

```bash
sudo apt-get install apt-transport-https ca-certificates dirmngr
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv E0C56BD4

echo "deb https://repo.clickhouse.com/deb/stable/ main/" | sudo tee \
    /etc/apt/sources.list.d/clickhouse.list
sudo apt-get update

sudo apt-get install -y clickhouse-server clickhouse-client

sudo service clickhouse-server start

```

##### Lets Take Some Notes from Installation Outputs 

- **User & Group :**  
  - groupadd -r clickhouse
  - useradd -r --shell /bin/false --home-dir /nonexistent -g **clickhouse clickhouse**
  - groupadd -r **clickhouse-bridge**
     useradd -r --shell /bin/false --home-dir /nonexistent -g **clickhouse-bridge clickhouse-bridge**
     chown --recursive clickhouse-bridge:clickhouse-bridge '/usr/bin/clickhouse-odbc-bridge'
     chown --recursive clickhouse-bridge:clickhouse-bridge '/usr/bin/clickhouse-library-bridge'

- **Config Files**
  - Will set ulimits for clickhouse user in **/etc/security/limits.d/clickhouse.conf**
  - Config File :  **/etc/clickhouse-server/config.xml**
  - Users config file **/etc/clickhouse-server/users.xml**

- **Primary Folder**

  - Log directory **/var/log/clickhouse-server/** 
  - Data directory **/var/lib/clickhouse/** 
  - pid directory **/var/run/clickhouse-server**.

- Binary Tools

  in `/usr/bin` folder, you can find : 

  - **clickhouse-server** 
  - **clickhouse-client** 
  - **clickhouse-local**
  - **clickhouse-benchmark** 
  - **clickhouse-benchmark** 
  - **clickhouse-copier** 
  - **clickhouse-obfuscator** 
  - **clickhouse-compressor**
  - **clickhouse-format** 
  - **clickhouse-extract-from-config** 



- **HW** : change `default` user password 

  

##### First Steps

go to `Section 20` folder in CLI and type in these commands into the `terminal` :

```sql
$  clickhouse-client --password  default123
:) show databases;

SHOW DATABASES
Query id: 58a80cbb-0d30-470d-89f1-89079c9dc7ee

┌─name────┐
│ default │
│ system  │
└─────────┘
2 rows in set. Elapsed: 0.023 sec.

:) use system ; 
Query id: a51a91d2-b389-49b0-b053-65ae7690af54
Ok.
0 rows in set. Elapsed: 0.001 sec.
:) show tables ; 
SHOW TABLES
Query id: b0a97ee1-250b-4f75-9184-caa4b1e41d12
┌─name───────────────────────────┐
│ aggregate_function_combinators │
│ asynchronous_metric_log        │
│ asynchronous_metrics           │
│ build_options                  │
│ clusters                       │
│ collations                     │
│ columns                        │
│ contributors                   │
│ current_roles                  │
│ data_skipping_indices          │
│ data_type_families             │
│ databases                      │
│ detached_parts                 │
│ dictionaries                   │
│ disks                          │
│ distributed_ddl_queue          │
│ distribution_queue             │
│ enabled_roles                  │
│ errors                         │
│ events                         │
│ formats                        │
│ functions                      │
│ grants                         │
│ graphite_retentions            │
│ licenses                       │
│ macros                         │
│ merge_tree_settings            │
│ merges                         │
│ metric_log                     │
│ metrics                        │
│ models                         │
│ mutations                      │
│ numbers                        │
│ numbers_mt                     │
│ one                            │
│ part_moves_between_shards      │
│ parts                          │
│ parts_columns                  │
│ privileges                     │
│ processes                      │
│ projection_parts               │
│ projection_parts_columns       │
│ query_log                      │
│ query_thread_log               │
│ quota_limits                   │
│ quota_usage                    │
│ quotas                         │
│ quotas_usage                   │
│ replicas                       │
│ replicated_fetches             │
│ replicated_merge_tree_settings │
│ replication_queue              │
│ rocksdb                        │
│ role_grants                    │
│ roles                          │
│ row_policies                   │
│ settings                       │
│ settings_profile_elements      │
│ settings_profiles              │
│ stack_trace                    │
│ storage_policies               │
│ table_engines                  │
│ table_functions                │
│ tables                         │
│ time_zones                     │
│ trace_log                      │
│ user_directories               │
│ users                          │
│ warnings                       │
│ zeros                          │
│ zeros_mt                       │
└────────────────────────────────┘

71 rows in set. Elapsed: 0.004 sec.

:) desribe roles;
DESCRIBE TABLE  roles
Query id: de36e7d4-dacc-4e1e-8ef4-21a15029b71e

┌─name────┬─type───┬─default_type─┬─default_expression─┬─comment─┬─codec_expression─┬─ttl_expression─┐
│ name    │ String │              │                    │         │                  │                │
│ id      │ UUID   │              │                    │         │                  │                │
│ storage │ String │              │                    │         │                  │                │
└─────────┴────────┴──────────────┴────────────────────┴─────────┴──────────────────┴────────────────┘
3 rows in set. Elapsed: 0.001 sec.

:) show
Syntax error: failed at position 5 (end of query):
show
Expected one of: TABLES, CLUSTER, CHANGED, GRANTS, CREATE, ACCESS, QUOTA, SETTINGS, CURRENT ROLES, PRIVILEGES, PROCESSLIST, CLUSTERS, DATABASES, CURRENT QUOTA, ENABLED ROLES, CREATE, DICTIONARIES, USERS, ROLES, SETTINGS PROFILES, PROFILES, ROW POLICIES, POLICIES, QUOTAS

:) create 

Syntax error: failed at position 7 (end of query):
create
Expected one of: FUNCTION, VIEW, LIVE, DATABASE, USER, PROFILE, TEMPORARY, TABLE, ROW POLICY, POLICY, DICTIONARY, QUOTA, OR REPLACE, ROLE, SETTINGS PROFILE, MATERIALIZED


```

#### Let's Create a sample Database/Table

```sql
:) CREATE DATABASE test;
Query id: 0fcb5241-1977-43c6-80e1-6a441522aabe
Ok.
0 rows in set. Elapsed: 0.023 sec.

:) USE test;
...
:) CREATE TABLE test_tbl (\
  id UInt16,\
  create_time Date,\
  comment Nullable(String)\
) ENGINE = MergeTree()\
PARTITION BY toYYYYMM(create_time)\
ORDER BY  (id, create_time)\
PRIMARY KEY (id, create_time)\
TTL create_time + INTERVAL 3 MONTH\
SETTINGS index_granularity=8192;
     
:) Insert  into test_tbl values (0,'2021-10-12', null);
:) Insert  into test_tbl values (0,'2021-10-13', null);
:) Insert  into test_tbl values (1,'2021-09-13', null);
:) Insert  into test_tbl values (1,'2021-08-13', null);
:) Insert  into test_tbl values (2,'2021-10-14', null);

:) SELECT *
FROM test_tbl ; 


:)SELECT \
    toYear(create_time) AS Year,\
    toMonth(create_time) AS Month,\
    count(*) AS cnt\
FROM test_tbl \
GROUP BY\
    Year,\
    Month


:) Show Create table test_tbl ; 
CREATE TABLE test.test_tbl
(
    `id` UInt16,
    `create_time` Date,
    `comment` Nullable(String)
)
ENGINE = MergeTree
PARTITION BY toYYYYMM(create_time)
PRIMARY KEY (id, create_time)
ORDER BY (id, create_time)
SETTINGS index_granularity = 8192;

:) q
Bye.
```

> #### [Primary Keys and Indexes in Queries](https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/mergetree/#primary-keys-and-indexes-in-queries)
>
> Take the `(CounterID, Date)` primary key as an example. In this case, the sorting and index can be illustrated as follows:
>
> ```bash
>   Whole data:     [---------------------------------------------]
>   CounterID:      [aaaaaaaaaaaaaaaaaabbbbcdeeeeeeeeeeeeefgggggggghhhhhhhhhiiiiiiiiikllllllll]
>   Date:           [1111111222222233331233211111222222333211111112122222223111112223311122333]
>   Marks:           |      |      |      |      |      |      |      |      |      |      |
>                   a,1    a,2    a,3    b,3    e,2    e,3    g,1    h,2    i,1    i,3    l,3
>   Marks numbers:   0      1      2      3      4      5      6      7      8      9      10
> ```



####  View  Data Folder 

```bash
$ sudo su -
$ cd /var/lib/clickhouse/
$ ls
access   cores  dictionaries_lib  format_schemas  metadata_dropped      status  tmp   user_files    uuid backups  data   flags   metadata  preprocessed_configs  store   user_defined  user_scripts
$ cd data
$ ls 
default  system  test
$ cd test	
$ ls
test_tbl
$ cd test_tbl
$ ls
202110_1_5_1  detached  format_version.txt
$ cd 202110_1_5_1
$ ls
checksums.txt  count.txt  data.mrk3                      minmax_create_time.idx  primary.idx
columns.txt    data.bin   default_compression_codec.txt  partition.dat           ttl.txt
$ exit 
```

- do not partition data by a high cardinality field! -> lots of folders will be created! 

  > Partitioning does not speed up queries (in contrast to the ORDER BY expression). You should never use too granular partitioning. Don't partition your data by client identifiers or names (instead make client identifier or name the first column in the ORDER BY expression).
  
  if you curious about the partition file name , [consider this example](https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/custom-partitioning-key/) :
  
  > Let’s break down the name of the first part: 201901_1_3_1: 
  >
  > 201901 is the partition name.
  > 1 is the minimum number of the data block.
  > 3 is the maximum number of the data block.
  > 1 is the chunk level (the depth of the merge tree it is formed from).

#### Dbeaver (GUI Tool)

- start `DBveaver` and create new `Clickhouse` connection 
- open script editor and type in the followings : 

```sql
Create database advertise; 
Use Advertise ; 
CREATE TABLE daily_stats
(
    id  UInt64  Codec(T64),
    viewed UInt64  Codec(T64),
    clicked UInt64 Codec(T64) ,
    amount UInt64 Codec(T64) ,
    summary_datetime Date,
    campaign_id UInt32 ,
    advertiser_id UInt32 Codec(T64),
    publisher_id UInt32 Codec(T64)
 )
ENGINE = MergeTree()
PARTITION BY toYYYYMM(summary_datetime)
ORDER BY (advertiser_id, campaign_id, publisher_id, summary_datetime)
settings index_granularity = 8192;

```

#### Import Data in CLI

- unzip `Data/daily_stats_100k.zip` 

- open a terminal (command prompt), then go into `Section 20` folder . 

```bash
$ clickhouse-client --password default123 --query="INSERT INTO advertise.daily_stats  FORMAT CSVWithNames" < Data/daily_stats_100k.csv
```

#### SQL Queries 

```sql
select count (*)
from daily_stats ds; 

select *
from daily_stats ds ;

select count (distinct advertiser_id),count (distinct publisher_id) 
from daily_stats ds 


select advertiser_id , campaign_id , count(distinct publisher_id) as cnt
from daily_stats ds 
group by advertiser_id , campaign_id
order by cnt desc 
;

select toDayOfMonth(summary_datetime) as day, sum(amount)
from daily_stats ds 
where advertiser_id = 105 and toYYYYMM(summary_datetime)=202007
group by toDayOfMonth(summary_datetime)

select toDayOfMonth(summary_datetime) as day,campaign_id , sum(amount) as amount, sum(clicked) as clicked , sum(viewed) as viewed 
from daily_stats ds 
where advertiser_id = 105 and toYYYYMM(summary_datetime)=202007
group by toDayOfMonth(summary_datetime), campaign_id 
order by day, amount desc ;

```

#### Update/Delete

```sql
delete from daily_stats ;
## Error!
alter table daily_stats 
delete where  1=1 ;

 INSERT INTO advertise.daily_stats
(id, viewed, clicked, amount, summary_datetime, campaign_id, advertiser_id, publisher_id)
VALUES(2345, 1, 2, 3, '2020-12-12', 1, 2, 3);

select *
from daily_stats ds ;

alter table daily_stats 
UPDATE viewed=200, clicked=45 where id=2345;
```



##### Time Functions 

reference : [Dates and Times | ClickHouse Documentation](https://clickhouse.com/docs/en/sql-reference/functions/date-time-functions/)

- now
- toYear
- toMonth
- toDayOfMonth
- toDayOfYear
- toDayOfWeek
- toHour
- toMinute
- toStartOfYear
- toStartOfDay
- toStartOfHour
- toStartOfMinute
- toStartOfFiveMinute
- toYearWeek
- date_trunc
- date_add 
- toYYYYMM
- toYYYYMMDD
- toYYYYMMDDhhmmss

```sql
WITH
    toDate('2018-01-01') AS date,
    toDateTime('2018-01-01 00:00:00') AS date_time
SELECT
    addYears(date, 1) AS add_years_with_date,
    addYears(date_time, 1) AS add_years_with_date_time
```



#### Some Useful Stats 

```sql\
SELECT
    partition,
    name,
    active
FROM system.parts
WHERE table = 'daily_stats';


INSERT INTO advertise.daily_stats
(id, viewed, clicked, amount, summary_datetime, campaign_id, advertiser_id, publisher_id)
VALUES(1425, 70, 80, 9000, '2021-10-10', 105, 106, 107);

SELECT
    partition,
    name,
    active
FROM system.parts
WHERE table = 'daily_stats';

optimize table daily_stats final; 

SELECT table,
    formatReadableSize(sum(bytes)) as size,
    min(min_date) as min_date,
    max(max_date) as max_date
    FROM system.parts
    WHERE active
GROUP BY table;


SELECT table, formatReadableSize(size) as size, rows, days, formatReadableSize(avgDaySize) as avgDaySize FROM (
    SELECT
        table,
        sum(bytes) AS size,
        sum(rows) AS rows,
        min(min_date) AS min_date,
        max(max_date) AS max_date,
        (max_date - min_date) AS days,
        size / (max_date - min_date) AS avgDaySize
    FROM system.parts
    WHERE active
    GROUP BY table
    ORDER BY rows DESC
);



```

