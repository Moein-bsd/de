from datetime import datetime
import json
from kafka import KafkaProducer
import random
import time
import uuid
from faker import Faker

faker = Faker()

EVENT_TYPE_LIST = ['buy', 'click','hover', 'idle_5']

start_date = datetime.date(2020, 1, 1)
end_date = datetime.date(2021, 10, 30)

time_between_dates = end_date - start_date
days_between_dates = time_between_dates.days


producer = KafkaProducer(
   value_serializer=lambda msg: json.dumps(msg).encode('utf-8'), # we serialize our data to json for efficent transfer
   # bootstrap_servers=['kafka1:9091','kafka2:9092','kafka3:9093'],
   bootstrap_servers=['localhost:9092'],
   key_serializer=str.encode)

TOPIC_NAME = 'events_topic'

usernames=[ faker.user_name() for x in range(1000)]
def _produce_event():
    """
    Function to produce events
    """
    # UUID produces a universally unique identifier
    random_number_of_days = random.randrange(days_between_dates)
    random_datetime = start_date + datetime.timedelta(days=random_number_of_days)
    return {
        'event_id': str(uuid.uuid4()),
        'event_datetime': random_datetime,
        'event_type': random.choice(EVENT_TYPE_LIST),
        'user_name': random.choice(usernames),
        'page': faker.uri_path()

    }

def send_events():
    while(True):
        data = _produce_event()
        time.sleep(1) # simulate some processing logic
        producer.send(TOPIC_NAME, value=data, key=data['event_id'])
        print(f"Event Created : {data['event_id']}")

if __name__ == '__main__':
    send_events()