---

## Postgres Monitoring

for monitoring Postgres using Grafana , we need 

- Grafana
- Prometheus
- Postgres-exporter (that points to a Postgres database )
- Postgres

Use This Steps :

- shut down the step1 Grafana cluster: 
  -  `docker-compose down -v`
-  start the step2 cluster 
  - check out the `docker-compose.yml` file
  - check out the `prometheus.yml`
  - add `prometheus` data source to the Grafana
  - import dashboard ID :  9628  (https://grafana.com/grafana/dashboards/9628)
  - create a sample db and run some queries
  - check out the newly imported dashboard.