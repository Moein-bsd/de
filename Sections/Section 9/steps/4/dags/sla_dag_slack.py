from airflow import DAG
from airflow.models.taskinstance import TaskInstance
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.providers.slack.operators.slack_webhook import SlackWebhookOperator
from airflow.hooks.base_hook import BaseHook
from airflow import AirflowException
from datetime import timedelta, datetime
from os import path
import logging
from airflow.operators.slack_operator import SlackAPIPostOperator
import time
from textwrap import dedent
from typing import Any, Dict, List, Optional, Tuple


logger = logging.getLogger("airflow.task")


SLACK_STATUS_TASK_FAILED = ":red_circle: Task Failed"
SLACK_STATUS_EXECUTION_TIMEOUT = ":alert: Task Failed by Execution Timeout."

def slack_failed_task(context):  
    failed_alert = SlackAPIPostOperator(
        task_id='SLA_Failed',
        channel="#sla_miss",
        token="/T01QF93UE4T/B01QFA1B6V9/wMG2CYyZ2zcwRXlFh7TG0HAO",
        text = ':red_circle: Task Failed',
        username = 'mojtaba.banaie',)
    return failed_alert.execute(context=context)


def send_slack_alert_sla_miss(
        dag: DAG,
        task_list: str,
        blocking_task_list: str,
        slas: List[Tuple],
        blocking_tis: List[TaskInstance],
) -> None:
    """Send `SLA missed` alert to Slack"""
    task_instance: TaskInstance = blocking_tis[0]
    message = dedent(
        f"""
        :warning: Task SLA missed.
        *DAG*: {dag.dag_id}
        *Task*: {task_instance.task_id}
        *Execution Time*: {task_instance.execution_date.strftime("%Y-%m-%d %H:%M:%S")} UTC
        *SLA Time*: {task_instance.task.sla}
        _* Time by which the job is expected to succeed_
        *Task State*: `{task_instance.state}`
        *Blocking Task List*: {blocking_task_list}
        *Log URL*: {task_instance.log_url}
        """
    )
    send_slack_alert(message=message)

def send_slack_alert(
        message: str,
        context: Optional[Dict[str, Any]] = None,
) -> None:
    """Send prepared message to Slack"""
    slack_webhook_token = BaseHook.get_connection("slack").password
    notification = SlackWebhookOperator(
        task_id="slack_notification",
        http_conn_id="slack",
        webhook_token=slack_webhook_token,
        message=message,
        username="mojtaba.banaie",
    )
    notification.execute(context)




default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2020, 1, 1, 23, 15, 0),
    'email': None,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0
}



EXCEL_FILE_PATH = "/tmp"
EXCEL_FILE_EXT = "xlsx"

with DAG('sla_dag_slack', default_args=default_args, sla_miss_callback=send_slack_alert_sla_miss, schedule_interval="*/1 * * * *", catchup=False) as dag:

    t0 = DummyOperator(task_id='final_task')

    # t1 = BashOperator(task_id='t1', bash_command='sleep 15', sla=timedelta(seconds=5), retries=0)

    task_read_stock_exchange_xlsx_file = BashOperator(
        task_id='Download-Stock-Exchange-Xlsx-File_2',
        bash_command='curl --retry 10 --output {0} -L -H "User-Agent:Chrome/61.0" --compressed "http://members.tsetmc.com/tsev2/excel/MarketWatchPlus.aspx?d=0"'.format(
            path.join(EXCEL_FILE_PATH, "{0}_{1}.{2}".format('daily_trades',  datetime.now().strftime("%Y_%m_%d_%H_%M_%S"), EXCEL_FILE_EXT))),
        sla=timedelta(seconds=5),
    )



    task_read_stock_exchange_xlsx_file >> t0
